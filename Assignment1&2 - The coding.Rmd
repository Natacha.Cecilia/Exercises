---
title: "Assignment 1 - Language Development in ASD - part 2""
author: "Natacha Walker"
date: "26 sep 2018"
output: word_document
---

### Loading the relevant libraries

```{r Load Libraries, include = FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(pacman)
p_load(tidyverse, ggplot2, nlme, dplyr, lme4, MuMIn, lmerTest)

```

### Define your working directory and load the data

```{r}

getwd()

ASDdata <- read.csv("ASDdata.csv")

```

### Characterize the participants (Exercise 1)

```{r descriptive stats, include = TRUE}

TD <- filter(ASDdata, Diagnosis == "TD") %>%
  distinct(SUBJ)
ASD <- filter(ASDdata, Diagnosis == "ASD") %>%
  distinct(SUBJ)

gender_diagnosis <- distinct(ASDdata, SUBJ, Gender, Diagnosis) %>% dplyr::count(Diagnosis, Gender)

ethnicity <- distinct(ASDdata, SUBJ, Ethnicity, Diagnosis) %>% dplyr::count(Diagnosis, Ethnicity)

age_diagnosis <- distinct(ASDdata, SUBJ, Age, Diagnosis) %>% dplyr::count(Diagnosis, Age)

hist(ASDdata$Age)

```

## Let's test hypothesis 1: Children with ASD display a language impairment  (Exercise 2)

### Hypothesis: The child's MLU changes: i) over time, ii) according to diagnosis

Mixed effect linear model

```{r ex2, include = TRUE}

mixedmodel <- lmer(CHI_MLU ~ VISIT + Diagnosis +(1+VISIT|SUBJ), ASDdata)

summary(mixedmodel)

#Boxplot with facet_wrap function
fw_barplot <- ggplot(ASDdata, aes(x = VISIT, y = CHI_MLU, fill = Diagnosis)) +
geom_bar(stat = "summary", fun.y=mean) +
geom_errorbar(stat = "summary", fun.data = mean_se, width = 0.1) +
labs(title = "MLU for each visit", x = "Visit", y = "MLU") +
facet_wrap(~Diagnosis)

fw_barplot

```

Evaluation of the model

```{r ex2 evaluate, include = TRUE}

R2 <- r.squaredGLMM(mixedmodel) #Checking the R2 to see how much the model explains
R2

```

Using a growth curve model

```{r ex2 growth curve, include = TRUE}

mixedmodel2 <- lmer(CHI_MLU ~ VISIT + Diagnosis + I(VISIT^2)+(1+VISIT+I(VISIT^2)|SUBJ), ASDdata)

summary(mixedmodel2)

R2_growth <- r.squaredGLMM(mixedmodel2)
R2_growth


anova(mixedmodel, mixedmodel2) #Model two is way better than model one.

#Linear
ggplot(ASDdata,aes(VISIT,CHI_MLU,color=Diagnosis))+geom_point()+geom_smooth(method=lm,formula=y~poly(x,1))

#Kvadratical
ggplot(ASDdata,aes(VISIT,CHI_MLU,color=Diagnosis))+geom_point()+geom_smooth(method=lm,formula=y~poly(x,2))

```

## Let's test hypothesis 2: Parents speak equally to children with ASD and TD  (Exercise 3)

### Hypothesis: Parental MLU changes: i) over time, ii) according to diagnosis

```{r ex3, include = TRUE}

MLU_barplot <- ggplot(ASDdata, aes(x = VISIT, y = MOT_MLU, fill = Diagnosis)) +
geom_bar(stat = "summary", fun.y=mean) +
geom_errorbar(stat = "summary", fun.data = mean_se, width = 0.1) +
labs(title = "Parental MLU for each visit", x = "Visit", y = "MLU") +
facet_wrap(~Diagnosis)

MLU_barplot

mixedmodel_MOT <- lmer(MOT_MLU ~ VISIT + Diagnosis +(1+VISIT|SUBJ), ASDdata)

summary(mixedmodel_MOT)

R2_MOT <- r.squaredGLMM(mixedmodel_MOT)
R2_MOT

ggplot(ASDdata,aes(VISIT,MOT_MLU,color=Diagnosis))+geom_point()+geom_smooth(method=lm,formula=y~poly(x,1)) #Use 1 or 2 after the x

```

### Adding new variables (Exercise 4)

Your task now is to figure out how to best describe the children linguistic trajectory. The dataset contains a bunch of additional demographic, cognitive and clinical variables (e.g.verbal and non-verbal IQ). Try them out and identify the statistical models that best describes your data (that is, the children's MLU). Describe how you selected the best model and send the code to run the model to Malte (au540041@post.au.dk).


```{r ex4, include = TRUE}

mixedmodel_new <- lmer(CHI_MLU ~ VISIT + Diagnosis + verbalIQ1 + types_CHI + (1+VISIT|SUBJ), ASDdata)

summary(mixedmodel_new)

R2_new <- r.squaredGLMM(mixedmodel_new)
R2_new
