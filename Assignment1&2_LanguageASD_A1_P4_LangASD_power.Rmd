---
title: "Assignment 1 - Language Development in ASD - part 4"
author: "Natacha Walker"
date: "August 10, 2017"
output: html_document
---


## Welcome to the fourth exciting part of the Language Development in ASD exercise

In this exercise we will assess how many participants we would need to adequately replicate our findings (ensuring our sample size is adequate, our alpha at 0.05 and our beta at 0.8).

### Exercise 1

How much power does your study have (if your model estimates are quite right)?
- [GitHub]Load your dataset, fit your favorite model, assess power for your main effects and interactions of interest.
- Report the power analysis and comment on what you can (or cannot) use its estimates for.

```{r}

library(pacman)
p_load(tidyverse, ggplot2, nlme, dplyr, lme4, MuMIn, lmerTest, Metrics, stringr, plyr, dplyr, groupdata2, tidyr)
p_load(simr)

Training_data <- read.csv("ASDdata.csv")
Training_data <- Training_data %>% drop_na() #Deleting all the NAs

Training_data <- Training_data %>%
  mutate(Diagnosis = ifelse(Diagnosis %in% "TD", 1, 0))

model <- lmer(CHI_MLU ~ VISIT * Diagnosis + ADOS1 + (1+VISIT|SUBJ), Training_data)
summary(model)

powerV = powerSim(model,fixed("VISIT"), nsim=200)
powerV #95.50%
powerD = powerSim(model,fixed("Diagnosis"), nsim=200)
powerD #100 %
powerI = powerSim(model,fixed("VISIT:Diagnosis"), nsim=200)
powerI #100 %

```

High power = easy to detect an effect


### Exercise 2

How would you perform a more conservative power analysis?
- Identify and justify a minimum effect size for each of your relevant effects

- [GitHub] take the model from exercise 1 and replace the effects with the minimum effect size that you'd accept.

```{r}

fixef(model)["VISIT"] <- 0.5
fixef(model)["Diagnosis"] <- 2
fixef(model)["VISIT:Diagnosis"] <- 0.2

```

- [GitHub] assess the power curve by Child.ID, identifying an ideal number of participants to estimate each effect

```{r}

powerCurveV = powerCurve(model, fixed("VISIT"),along="SUBJ", nsim=200)
powerCurveV #At 17 rows (participant 3) it says 6 % but at 50 rows it says 100 % and the rest is 100 (Row 50 is 9 participants?) %

plot(powerCurveV)

powerCurveD = powerCurve(model, fixed("Diagnosis"),along="SUBJ", nsim=200)
powerCurveD # Here it becores 100 % at 52 participants but over the threshold of 80 at 24 (93.50%), however at a theshold at 95 is at participant 31.

plot(powerCurveD)

powerCurveI = powerCurve(model, fixed("VISIT:Diagnosis"),along="SUBJ", nsim=200)
powerCurveI #person 24 = 82.50

plot(powerCurveI)

```

- Report the power analysis and comment on what you can (or cannot) use its estimates for.


### Exercise 3

Assume you have only the resources to collect 30 kids (15 with ASD and 15 TDs). Identify the power for each relevant effect and discuss whether it's worth to run the study and why

```{r}

model30 <- extend(model, within = "SUBJ+Diagnosis+VISIT", n=15)

powerV30 = powerSim(model30,fixed("VISIT"), nsim=200)
powerV30 
powerD30 = powerSim(model30,fixed("Diagnosis"), nsim=200)
powerD30 
powerI30 = powerSim(model30,fixed("VISIT:Diagnosis"), nsim=200)
powerI30 

powerCurveV30 = powerCurve(model30, fixed("VISIT"),along="SUBJ", nsim=200)
powerCurveV30

plot(powerCurveV30)

powerCurveD30 = powerCurve(model30, fixed("Diagnosis"),along="SUBJ", nsim=200)
powerCurveD30

plot(powerCurveD30)

powerCurveI30 = powerCurve(model30, fixed("VISIT:Diagnosis"),along="SUBJ", nsim=200)
powerCurveI30

plot(powerCurveI30)


```

